const express = require('express');
const cors = require('cors');
const app = express();
const port = 9000;

app.use(express.json());
app.use(cors());

app.post('/users', (request, response) => {
    response.json({msg: 'User added'});
    response.status(500);
    console.log('POST');
});

app.listen(port);