const BASE_URL = "http://localhost:9000/";

window.addEventListener('load', function () {
    registerUser();
})

function registerUser() {
    const btn = document.getElementById('btn-register');
    btn.addEventListener('click', function (event) {
        event.preventDefault();
        const intputName = document.getElementById('name');
        const intputEmail = document.getElementById('email');
        const intputPassword = document.getElementById('password');
        const user = {
            'name': intputName.value,
            'email': intputEmail.value,
            'password': intputPassword
        }
        postRegisterUser(user)
            .then(response => {

            })
            .catch(console.error)
    })
}

function postRegisterUser(user) {
    return fetch(`${BASE_URL}users`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(user)
    })
        .then(response => response.json())
        .catch(console.error)
}